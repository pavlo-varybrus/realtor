import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoaderComponent } from './loader/loader.component';
import { PropertyCardComponent } from './property-card/property-card.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    LoaderComponent, PropertyCardComponent
  ],
  exports: [
    LoaderComponent, PropertyCardComponent
  ]
})
export class SharedModule { }
