import { Component, OnInit, Input } from '@angular/core';
import { PropertyDto } from 'src/app/models/property-models';

@Component({
    selector: 'app-property-card',
    templateUrl: 'property-card.component.html',
    styleUrls: ['property-card.component.scss']
})

export class PropertyCardComponent implements OnInit {
    @Input() property: PropertyDto;

    constructor() {
    }

    ngOnInit() {

    }
}
