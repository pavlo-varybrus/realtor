import { Component, OnInit, NgZone } from '@angular/core';
import { ElementRef, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { Bounds } from 'src/app/models/map-models';
import { PropertyDto, PropertyTypeImage } from 'src/app/models/property-models';
import { SearchResult } from 'src/app/search/search-panel/models';
import { } from 'googlemaps';
import { Observable } from 'rxjs/internal/Observable';
import { fromEvent, Subject } from 'rxjs';
import { auditTime } from 'rxjs/operators';
import { MapRepositoryUI } from './map-repository-ui';
import * as _ from 'lodash';

@Component({
    selector: 'app-map-component',
    templateUrl: './map-component.component.html',
    styleUrls: ['./map-component.component.scss']
})

export class MapComponentComponent implements OnInit {
    geocoder: any;
    @ViewChild('mapElement', { static: true }) mapElement: ElementRef;
    @Input() bounds: Bounds;
    @Output() boundsChange = new EventEmitter<Bounds>();
    mapRepositoryUI: MapRepositoryUI;
    currentPropertyList: PropertyDto[] = [];

    @Input() set propertyList(value: Array<PropertyDto>) {
        if (this.mapRepositoryUI) {
            this.fillPropertiesInMap(value);
        }
    }

    @Input() set searchResult(value: SearchResult) {
        if (value == null) {
            return;
        }
        this.mapRepositoryUI.centerByAdress(value.country, value.city);
    }

    constructor(private zone: NgZone) {
    }

    ngOnInit() {
        this.mapRepositoryUI = new MapRepositoryUI(this.mapElement.nativeElement, this.zone, this.emitBoundsChanges);
    }

    private emitBoundsChanges = () => {
        this.boundsChange.emit(this.mapRepositoryUI.getBounds());
    }

    public fillPropertiesInMap = (propertyList: PropertyDto[]) => {
        for (const property of this.currentPropertyList) {
            const exist = _.find(propertyList, { lat: property.lat, lng: property.lng });
            if (exist === undefined) {
                this.mapRepositoryUI.removeMarker(property);
            }
        }

        // add marker if it doesn't exist in MarkerList
        for (const property of propertyList) {
            const exist = _.find(this.currentPropertyList, { lat: property.lat, lng: property.lng });
            if (exist == null) {
                this.mapRepositoryUI.createMarkerByPropertyDto(property);
            }
        }

        this.currentPropertyList = [...propertyList];
    }
}
