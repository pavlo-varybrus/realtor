import { NgModule } from '@angular/core';
import { MapComponentComponent } from './map-component.component';

@NgModule({
    imports: [
    ],
    declarations: [
        MapComponentComponent
    ],
    exports: [MapComponentComponent]
})
export class MapComponentModule {

}
