import { NgZone } from '@angular/core';

import { PropertyDto, PropertyTypeImage } from 'src/app/models/property-models';
import { Bounds } from 'src/app/models/map-models';
import * as _ from 'lodash';

export class MapRepositoryUI {
    geocoder: google.maps.Geocoder;
    map: google.maps.Map;
    propertyMarkerList: Array<{ marker: google.maps.Marker, property: PropertyDto }> = [];
    currentPropertyList: PropertyDto[] = [];

    constructor(mapNativeElement: any, private zone: NgZone, private emitBoundsChanges) {
        this.geocoder = new google.maps.Geocoder();
        const mapStyle: Array<google.maps.MapTypeStyle> = [
            {
                featureType: 'administrative',
                elementType: 'geometry',
                stylers: [
                    {
                        visibility: 'off'
                    }
                ]
            },
            {
                featureType: 'poi',
                stylers: [
                    {
                        visibility: 'off'
                    }
                ]
            },
            {
                featureType: 'road',
                elementType: 'labels.icon',
                stylers: [
                    {
                        visibility: 'off'
                    }
                ]
            },
            {
                featureType: 'transit',
                stylers: [
                    {
                        visibility: 'off'
                    }
                ]
            }
        ];
        const mapProp: google.maps.MapOptions = {
            center: new google.maps.LatLng(46.482860, 30.735459),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: mapStyle
        };
        this.map = new google.maps.Map(mapNativeElement, mapProp);

        // google map is ready event
        google.maps.event.addListenerOnce(this.map, 'idle', () => {
            this.zone.run(() => {
                this.emitBoundsChanges();
            });
        });

        google.maps.event.addListener(this.map, 'dragend', () => {
            this.zone.run(() => {
                this.emitBoundsChanges();
            });
        });

        google.maps.event.addListener(this.map, 'zoom_changed', () => {
            this.zone.run(() => {
                this.emitBoundsChanges();
            });
        });
    }

    public removeMarker = (property: PropertyDto) => {
        const propertyMarker = _.find(this.propertyMarkerList, (x) => x.property.lat === property.lat && x.property.lng === property.lng);
        _.remove(this.propertyMarkerList, propertyMarker);
        propertyMarker.marker.setMap(null);

    }

    public createMarkerByPropertyDto = (property: PropertyDto) => {
        const image = {
            url: PropertyTypeImage.getImageByType(property.type),
            scaledSize: new google.maps.Size(32, 32),
        };
        const marker = new google.maps.Marker({
            position: { lat: property.lat, lng: property.lng },
            map: this.map,
            icon: image,
        });
        this.propertyMarkerList.push({ marker, property });
    }

    public getBounds = () => {
        const bounds: Bounds = {
            northEastLat: this.map.getBounds().getNorthEast().lat(),
            northEastLng: this.map.getBounds().getNorthEast().lng(),
            southWestLat: this.map.getBounds().getSouthWest().lat(),
            southWestLng: this.map.getBounds().getSouthWest().lng()
        };
        return bounds;
    }

    public centerByAdress = (country: string, city: string) => {
        if (country == null) {
            return;
        }
        let addres = `${country} ${city}`;
        if (city == null) {
            addres = addres = `${country}`;
        }

        this.geocoder.geocode({ address: addres }, (results: any, status: any) => {
            if (status === google.maps.GeocoderStatus.OK) {
                this.map.setCenter(results[0].geometry.location);
                this.map.fitBounds(results[0].geometry.viewport);
                this.emitBoundsChanges();
            }
        });
    }
}
