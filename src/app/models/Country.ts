import { City } from './City';
export class Country {
    id: number;
    name: string;
    cityList: Array<City>;
}
