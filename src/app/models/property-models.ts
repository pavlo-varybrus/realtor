import { find } from 'lodash';

export enum PropertyType {
    None,
    House,
    Appartment
}
export class PropertyDto {
    public id: number;
    public type: PropertyType;
    public lat: number;
    public lng: number;
    public titleImage: string;
    public rooms: number;
    public area: number;
    public location: { name: string, number: string };
    public price: number;
    public typeOperation: string;
}

export class PropertyTypeImage {
    static resource: Array<{ type: PropertyType, imgUrl: string }> = [
        { type: PropertyType.House, imgUrl: 'assets/house.png' },
        { type: PropertyType.Appartment, imgUrl: 'assets/building.png' }
    ];
    static getImageByType(type: PropertyType): string {

        return find(PropertyTypeImage.resource, { type }).imgUrl;
    }

    constructor() {

    }

}
