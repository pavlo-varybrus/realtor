export class Bounds {
    northEastLat: number;
    northEastLng: number;
    southWestLat: number;
    southWestLng: number;
}
