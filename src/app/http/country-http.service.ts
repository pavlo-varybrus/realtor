import { Injectable } from '@angular/core';
import { Country } from '../models/Country';
import { Observable, of } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CountryHttpService {
  constructor() {

  }

  public getCountryWithSity = (): Observable<Country[]> => {
    const data = [
      {
        id: 1, name: 'USA', cityList: [
          { id: 1, name: 'NY' },
          { id: 2, name: 'LA' }]
      }, {
        id: 2, name: 'Ukrain', cityList: [
          { id: 1, name: 'Odessa' },
          { id: 2, name: 'Kiev' },
          { id: 3, name: 'Lviv' }]
      }];

    return of<Country[]>(data);
  }

}
