import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { SearchComponent } from './search.component';
import { SearchPanelComponent } from './search-panel/search-panel.component';
import { ReactiveFormsModule, FormControl } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedModule } from '../shared';
import { MapComponentModule } from '../component/map-component/map-component.module';


@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        FormsModule,
        NouisliderModule,
        NgSelectModule,
        SharedModule,
        MapComponentModule
    ],
    declarations: [
        SearchComponent, SearchPanelComponent
    ],
    providers: [],
})
export class SearchModule {

}
