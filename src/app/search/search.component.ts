import { Component, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { filter } from 'lodash';
import { PropertyDto, PropertyType } from '../models/property-models';
import { Bounds } from '../models/map-models';


@Component({
    selector: 'app-search',
    templateUrl: 'search.component.html',
    styleUrls: ['search.component.scss']
})
export class SearchComponent {
    filteredPropertyList: PropertyDto[] = [];
    searchStatus: any;
    readonly allPropertyList = [
        // tslint:disable-next-line: max-line-length
        { id: 0, type: PropertyType.Appartment, lat: 46.482860, lng: 30.739985, area: 100, rooms: 1, bedroom: 2, location: { name: 'Grand hotel room 1', number: 'qwe' }, price: 110, titleImage: 'assets/appartment.jpg', typeOperation: 'sale' },
        // tslint:disable-next-line: max-line-length
        { id: 1, type: PropertyType.House, lat: 46.482545, lng: 30.733804, area: 101, rooms: 2, bedroom: 21, location: { name: 'Grand hotel room111 2', number: 'qwe1' }, price: 1101, titleImage: 'assets/listing-2.jpg', typeOperation: 'sale1' },
        // tslint:disable-next-line: max-line-length
        { id: 2, type: PropertyType.Appartment, lat: 46.488960, lng: 30.739985, area: 100, rooms: 3, bedroom: 2, location: { name: 'Grand hotel room 4', number: 'qwe' }, price: 110, titleImage: 'assets/appartment.jpg', typeOperation: 'sale' },
        // tslint:disable-next-line: max-line-length
        { id: 3, type: PropertyType.House, lat: 46.487545, lng: 30.733804, area: 101, rooms: 4, bedroom: 21, location: { name: 'Grand hotel room111 5', number: 'qwe1' }, price: 1101, titleImage: 'assets/listing-2.jpg', typeOperation: 'sale1' },
        // tslint:disable-next-line: max-line-length
        { id: 4, type: PropertyType.Appartment, lat: 46.486060, lng: 30.739985, area: 100, rooms: 5, bedroom: 2, location: { name: 'Grand hotel room 6', number: 'qwe' }, price: 110, titleImage: 'assets/appartment.jpg', typeOperation: 'sale' },
        // tslint:disable-next-line: max-line-length
        { id: 5, type: PropertyType.House, lat: 46.485145, lng: 30.733804, area: 101, rooms: 11, bedroom: 21, location: { name: 'Grand hotel room111 11', number: 'qwe1' }, price: 1101, titleImage: 'assets/listing-2.jpg', typeOperation: 'sale1' },
        // tslint:disable-next-line: max-line-length
        { id: 6, type: PropertyType.Appartment, lat: 46.484260, lng: 30.739985, area: 100, rooms: 1, bedroom: 2, location: { name: 'Grand hotel room 1', number: 'qwe' }, price: 110, titleImage: 'assets/appartment.jpg', typeOperation: 'sale' },
        // tslint:disable-next-line: max-line-length
        { id: 7, type: PropertyType.House, lat: 46.489545, lng: 30.733804, area: 101, rooms: 11, bedroom: 21, location: { name: 'Grand hotel room111 11', number: 'qwe1' }, price: 1101, titleImage: 'assets/listing-2.jpg', typeOperation: 'sale1' },
    ];

    constructor() {

    }

    mapBoundsChange = (bounds: Bounds) => {
        this.filteredPropertyList = filter(this.allPropertyList, (propery: PropertyDto) => {
            return propery.lat < bounds.northEastLat
                && propery.lat > bounds.southWestLat
                && propery.lng < bounds.northEastLng
                && propery.lng > bounds.southWestLng;
        });
    }

}
