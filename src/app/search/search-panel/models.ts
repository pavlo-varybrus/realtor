export class ItemElement {
    id: number;
    text: string;
}


export class SearchResult {
    country: string;
    city: string;
    maxRoom: number;
    minRoom: number;
    priceRange: [];
}
