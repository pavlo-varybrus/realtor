import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { ReactiveFormsModule, FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { map, startWith, tap } from 'rxjs/operators';
import { NgOption } from '@ng-select/ng-select';
import { find } from 'lodash';
import { SearchResult } from './models';
import { City } from 'src/app/models/City';
import { Country } from 'src/app/models/Country';
import { CountryHttpService } from 'src/app/http/country-http.service';



@Component({
    selector: 'app-search-panel',
    templateUrl: 'search-panel.component.html',
    styleUrls: ['search-panel.component.scss']
})
export class SearchPanelComponent implements OnInit {
    @Input() searchStatus: SearchResult;
    @Output() searchStatusChange = new EventEmitter<SearchResult>();
    cityList: City[];
    countryList: Country[];
    someRange2config: any = {
        behaviour: 'drag',
        connect: true,
        range: {
            min: 0,
            max: 20000
        },
        step: 1
    };
    searchForm: FormGroup;

    constructor(private fb: FormBuilder, private countryHttpService: CountryHttpService) {
        countryHttpService.getCountryWithSity()
        this.searchForm = this.fb.group({
            country: null,
            city: new FormControl(null, Validators.required),
            minRooms: null,
            maxRooms: null,
            priceRange: new FormControl([5000, 10000])
        });
    }

    ngOnInit() {

        this.searchForm.controls.country.valueChanges.subscribe(val => {
            const country = this.getCountryByValue(val);
            this.cityList = country && country.cityList;
            this.searchForm.controls.city.setValue(null);
            this.updatSearchResult();
        });

        this.searchForm.controls.city.valueChanges.subscribe(val => {
            this.updatSearchResult();
        });
    }

    updatSearchResult = () => {
        this.searchStatus = new SearchResult();
        const country = this.getCountryByValue(this.searchForm.controls.country.value);
        this.searchStatus.country = country == null ? null : country.name;
        const city = this.getCityByValue(this.searchForm.controls.city.value);
        this.searchStatus.city = city == null ? null : city.name;
        this.searchStatus.minRoom = parseInt(this.searchForm.controls.minRooms.value, 10);
        this.searchStatus.maxRoom = parseInt(this.searchForm.controls.maxRooms.value, 10);
        this.searchStatus.priceRange = this.searchForm.controls.priceRange.value;
        console.log(JSON.stringify(this.searchStatus));
        this.searchStatusChange.emit(this.searchStatus);
    }

    private getCountryByValue = (val: number): Country => {
        return find(this.countryList, { id: val });
    }

    private getCityByValue = (val: number): City => {
        return find(this.cityList, { id: val });
    }

}
